var express = require('express');
var router = express.Router();
var request = require('request');

router.use(function(req, res, next){
  var qs = Object.assign(req.query, {
    key: '9e1bc9e2e729e32e75a4afcacb6de309',
    form: 'json'
  });
  var url = 'http://api.brewerydb.com/v2'+req.path
  var options = {
    method: req.method,
    json: true,
    url: url,
    qs: qs
  }

  request(options, function (err, response, body) {
    if (err) {
      res.status(500).json({
        status: 500,
        error: err,
        message: "An error occured",
        data: body
      })
    } else {
      res.status(response.statusCode).json(body);
    }
  })
})

module.exports = router;
