var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.status(200).json({
    status: 200,
    message: "Welcome to the GeekNight API!"
  })
});

router.get('/helloworld', function(req, res, next) {
  res.status(200).json({
    status: 200,
    message: "Hello World from NodeJS!"
  })
});

router.get('/helloworld-timeout', function(req, res, next) {
  setTimeout(function(){
    res.status(200).json({
      status: 200,
      message: "Hello World from NodeJS!"
    })
  }, 2000);
});

module.exports = router;
