'use strict';
module.exports = (sequelize, DataTypes) => {
  var Analytics = sequelize.define('Analytics', {
    origin: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return Analytics;
};